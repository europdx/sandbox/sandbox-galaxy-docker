#!/bin/bash

echo "-------------------------------------------"
echo "Acceptable Use Policy and Conditions of Use"
echo "-------------------------------------------"
echo "Acceptable Use Policy and Conditions of Use (https://dataportal.europdx.eu/about/aup)"
echo "have to be accepted before downloading and using EurOPDX data."

read -p "Do you accept the Acceptable Use Policy and Conditions of Use? [yes/no]" choice
case "$choice" in 
  yes|Yes ) echo "AUP accepted.";;
  * ) echo "AUP not accepted. Exiting." ;exit 0;;
esac


set -x

# Copy graph.tgz
cp neo4j-seed-dbs/210701-seeddb-2.0.2-trace/graph.tgz neo4j-data
#wget -P neo4j-data http://dior.ics.muni.cz/~cuda/edirex/sandbox/graph.tgz 

# Initial database files for cBioPortal
#wget --backups=1 -P cbioportal/mysql-snapshots/initialized_no_studies https://gitlab.ics.muni.cz/europdx/cbioportal/cbioportal-docker-compose/-/raw/master/mysql-snapshots/initialized_no_studies/cbipportal_mysql.tgz
wget --backups=1 -P cbioportal/mysql-snapshots/initialized_2_studies https://gitlab.ics.muni.cz/europdx/cbioportal/cbioportal-docker-compose/-/raw/master/mysql-snapshots/initialized-2-studies/cbipportal_mysql.tgz

#mkdir -p cbioportal/cbioportal_mysql_data
#tar -xf cbioportal/mysql-snapshots/initialized_no_studies/cbipportal_mysql.tgz -C cbioportal/cbioportal_mysql_data

set +e

rm -rf cbioportal/study/* || sudo rm -rf cbioportal/study/*

docker rm -f copy

#Prepare volume for neo4j seed data
docker volume rm neo4j_data_volume
docker volume create neo4j_data_volume
docker run -d --name copy --mount source=neo4j_data_volume,target=/data nginx
docker cp neo4j-data/graph.tgz copy:/data
docker cp neo4j-data/neo4j-init.sh copy:/data
docker rm -f copy


#Prepare volume for cbioportal database 
docker volume rm cbioportal_mysql_data_volume
docker volume create cbioportal_mysql_data_volume
docker run -d --name copy --mount source=cbioportal_mysql_data_volume,target=/data nginx
#docker cp cbioportal/mysql-snapshots/initialized_no_studies/cbipportal_mysql.tgz copy:/
docker cp cbioportal/mysql-snapshots/initialized_2_studies/cbipportal_mysql.tgz copy:/
docker exec copy /bin/bash -c "tar -xf /cbipportal_mysql.tgz -C /data"
docker rm -f copy

# Prepare volume for cbioporatal config
docker volume rm cbioportal_config_volume
docker volume create cbioportal_config_volume
docker run -d --name copy --mount source=cbioportal_config_volume,target=/data nginx
docker cp cbioportal/config/portal.properties copy:/
docker cp cbioportal/config/logo_EurOPDX.png copy:/
docker rm -f copy
