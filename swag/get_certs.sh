#!/bin/bash

#!/bin/bash

MY_HOST=$(grep ^HOST= ../.env|sed s/HOST=//)
MY_PATH=config/etc/letsencrypt/live/$MY_HOST
PROXY_DIR=../proxy/ssl
echo $MY_HOST

sed -i 's/HOST_PLACE_HOLDER/$MY_HOST/g' docker-compose.yml

docker-compose up -d

sleep 20

if [[ -f $MY_PATH/privkey.pem && -f $MY_PATH/privkey.pem ]];
then
    echo "SSL certificate created."
    # Backup old files
    cp $PROXY_DIR/fullchain.pem $PROXY_DIR/fullchain.backup
    cp $PROXY_DIR/privkey.pem $PROXY_DIR/privkey.backup
    # Copy to proxy directory
    cp $MY_PATH/fullchain.pem $PROXY_DIR
    cp $MY_PATH//privkey.pem $PROXY_DIR
else
    echo "SSL certificate not generated. Using self-signed SSL certificate."
fi
docker-compose down