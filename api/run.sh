#!/bin/bash

# source venv/bin/activate

# Start API process
python3 api.py &> /api/log/api.log&

# Start Worker process
rq worker -u redis://redis:6379 sandbox-api &> /api/log/worker.log&

# Wait for any process to exit
wait -n
  
# Exit with status of process that exited first
exit $?