#!/bin/bash


set -x
HOME_DIR=/root
COMPOSE_DIR=/sandbox-galaxy-docker
DOCUMENTS=$HOME_DIR/Documents
IMPORT_FOLDER=/tmp/import

# Make sure emty ~/Documents directory exists
mkdir -p $DOCUMENTS/pdx.graphdb
rm -rfv $DOCUMENTS/pdx.graphdb/*

# Extract seed db to ~/Documents/pdx.graphdb
tar -zxvf $COMPOSE_DIR/neo4j-seed-dbs/210701-seeddb-2.0.2-trace/graph.tgz --directory $DOCUMENTS/pdx.graphdb

# Launch loader
cd $HOME_DIR/dataportal-sourcecode
java -jar indexer/target/indexer-1.0.0.jar load --group=EurOPDX --data-dir=$IMPORT_FOLDER/europdx


# Tar created N4J DB files
cd $HOME_DIR/Documents/pdx.graphdb
tar -czvf ../graph.tgz *


#Move graph.tgz to $COMPOSE_DIR/neo4j-data
#mv -f ../graph.tgz  $COMPOSE_DIR/neo4j-data 

#Stop selected containers from sandbox stack
cd $COMPOSE_DIR
docker-compose stop dataportal
docker-compose rm -f dataportal

docker-compose stop neo4j
docker-compose rm -f neo4j

# Start copy container, mount neo4j_data_volume and copy graph.tgz to volume
docker run -d --name copy --mount source=neo4j_data_volume,target=/data nginx
docker cp $HOME_DIR/Documents/graph.tgz copy:/data
docker rm -f copy

# Delete volume with neo4j data volume
docker volume rm sandbox-galaxy-docker_neodata

sleep 15

# Start stopped containers
docker-compose up -d --no-recreate


# DS - init DataHub
sleep 30
#cd ~/datahub-docker/api/
#./init_datahub.sh
docker exec  datahub bash -c "sed -i 's/localhost/db/g' config.py"
docker exec datahub ./init_datahub_from_container.sh


# Import data to cBioPortal
cd /api/app
./load-custom-study.sh

cd $COMPOSE_DIR
docker-compose restart proxy