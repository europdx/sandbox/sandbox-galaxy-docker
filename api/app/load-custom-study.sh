#!/bin/bash

set -x

HOME_DIR=/root
COMPOSE_DIR=/sandbox-galaxy-docker

export ID=`curl -X POST -H "Content-Type: application/json" -k http://datahub:5000/api/v1/tmplists/NKIX | jq '.tmplistid'`

if [ $ID = "null" ]
then
    echo "There are no pdxmodels for NKIX study"
else
    echo $ID
    mkdir _studies
    rm -r _studies/*

    # create files for cBioPortal
    $HOME_DIR/data-providing/cbio_client.py 

    # move the files to cBioPortal folder
    rm -r $COMPOSE_DIR/study/*
    cp -r _studies/* $COMPOSE_DIR/cbioportal/study/

    # run cBioPortal's importer
    cd $COMPOSE_DIR/cbioportal
    ./import_studies.sh
fi
