from flask import Flask
from flask_restful import Api

from config import Config

app = Flask(__name__)

app.config.from_object(Config)
app.app_context().push()
api = Api(app)

from app import routes
