import os
import tarfile
import shutil
import logging
from flask import jsonify, request, abort, Response
from redis import Redis
import rq

from app import app
from app.tools import init_folder, set_status_value, get_status_value
from flask_httpauth import HTTPTokenAuth
from config import Config

auth = HTTPTokenAuth(scheme='Bearer')

tokens = {
    "edirex": "Administrator"
}


@auth.verify_token
def verify_token(token):
    if token in tokens:
        return tokens[token]


#@app.before_first_request
#def before_first_request_func():
#    init_folder(app.config['UPLOAD_FOLDER'])
#    init_folder(app.config['IMPORT_FOLDER'])


@app.errorhandler(404)
def page_not_found(e):
    return '<h1>Error 404</h1><b>Sandbox API</b><p>More to be found in  <a href="' + app.config['USAGE_INFO_ADDRESS'] + '"> documentation</a>.</p>', 404


@app.route('/')
@auth.login_required
def index():
    return "Hello, {}!".format(auth.current_user())


@app.route(app.config['PREFIX'] + '/status', methods=['GET'])
@auth.login_required
def get_status():
    """
    :return:  sandbox status
    """
    return jsonify(status=get_status_value())

@app.route(app.config['PREFIX'] + '/log/reset', methods=['GET'])
@auth.login_required
def get_reset_log():
    """
    :return:  sandbox reset log
    """
    log = "***** Reset stdout log *****\n"


    if os.path.isfile(app.config['RESET_STDOUT']):
        with open(app.config['RESET_STDOUT'], 'r') as stdout_file:
            log += stdout_file.read()
    else:
        log +="Nothing in stdout.\n"

    log += "\n***** Reset stderr log *****\n"

    if os.path.isfile(app.config['RESET_STDERR']):
        with open(app.config['RESET_STDERR'], 'r') as stderr_file:
            log += stderr_file.read()
    else:
        log +="Nothing in stderr.\n"

    return log


@app.route(app.config['PREFIX'] + '/log/import', methods=['GET'])
@auth.login_required
def get_import_log():
    """
    :return:  sandbox import log
    """
    log = "***** Import stdout log *****\n"

    if os.path.isfile(app.config['IMPORT_STDOUT']):
        with open(app.config['IMPORT_STDOUT'], 'r') as stdout_file:
            log += stdout_file.read()
    else:
        log += "Nothing in stdout.\n"

    log += "\n***** Import stderr log *****\n"

    if os.path.isfile(app.config['IMPORT_STDERR']):
        with open(app.config['IMPORT_STDERR'], 'r') as stderr_file:
            log += stderr_file.read()
    else:
        log += "Nothing in stderr.\n"

    return log


@app.route(app.config['PREFIX'] + '/import', methods=['POST'])
@auth.login_required
def import_data():
    """
    Starts import of data from import directory
    :return:  sandbox status
    """

    set_status_value("importing")
    queue = rq.Queue('sandbox-api', connection=Redis.from_url(app.config['REDIS_URL']))
    job = queue.enqueue('app.tasks.import_data', job_timeout=5000) # Probably it will be necessary to increase the timeout if imported bigger data 

#    tgz_file_full_path = os.path.join(app.config['UPLOAD_FOLDER'], app.config['UPLOAD_FILE'])
#    if os.path.isfile(tgz_file_full_path):
#        tar = tarfile.open(tgz_file_full_path)
#        tar.extractall(path=app.config['IMPORT_FOLDER'])
#        tar.close()
#        logging.info("Tgz file extracted.")
#    else:
#        resp = jsonify(status="failed", message="Tgz file not available.")
#        resp.status_code = 400
#        return resp


    resp = jsonify(status="ok", message="Import initialized.")
    resp.status_code = 200
    return resp



@app.route(app.config['PREFIX'] + '/reset', methods=['POST'])
@auth.login_required
def reset():
    """
    Starts reset of sandbox environment
    :return:  sandbox status
    """
    set_status_value("resetting")
    queue = rq.Queue('sandbox-api', connection=Redis.from_url(app.config['REDIS_URL']))
    job = queue.enqueue('app.tasks.reset_sandbox')
    logging.info("Reset initialized.")
    resp = jsonify(status="ok", message="Reset initialized.")
    resp.status_code = 200
    return resp


@app.route(app.config['PREFIX'] + '/upload', methods=['POST'])
@auth.login_required
def upload_data():
    """
    Uploads file with data.
    :return:  sandbox status
    """
    # check if the post request has the file part
    if 'file' not in request.files:
        logging.error("No file part in the request")
        resp = jsonify({'message': 'No file part in the request'})
        resp.status_code = 400
        return resp
    file = request.files['file']
    if file.filename == '':
        logging.error("No file selected for upload.")
        resp = jsonify({'message': 'No file selected for upload.'})
        resp.status_code = 400
        return resp

    filename = file.filename
    try:
        os.makedirs(app.config['UPLOAD_FOLDER'],mode=0o755)
    except FileExistsError:
        pass

    file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
    logging.info("File uploaded.")
    resp = jsonify({'message': 'File successfully uploaded'})
    resp.status_code = 201
    return resp


