#!/bin/bash
set -x

whoami

HOME_DIR=/root
COMPOSE_DIR=/sandbox-galaxy-docker
CBIO_DIR=$HOME_DIR/cbioportal-docker-compose

#TEST_STUDY=https://gitlab.ics.muni.cz/europdx/sandbox/data/-/raw/master/test-study.zip?inline=false

# Stop and remove some of the containers
cd $COMPOSE_DIR

docker-compose stop dataportal
docker-compose rm -f dataportal

docker-compose stop db
docker-compose rm -f db

docker-compose stop datahub
docker-compose rm -f datahub

docker-compose stop neo4j
docker-compose rm -f neo4j

docker-compose stop cbioportal

docker-compose stop cbioportal-database
docker-compose rm -f cbioportal-database

#sleep 15
docker volume rm sandbox-galaxy-docker_neodata
docker volume rm sandbox-galaxy-docker_dbdata

#Prepare volume for neo4j seed data
docker volume rm neo4j_data_volume
docker volume create neo4j_data_volume
docker run -d --name copy --mount source=neo4j_data_volume,target=/data nginx
docker cp neo4j-seed-dbs/210701-seeddb-2.0.2-trace/graph.tgz copy:/data
docker cp neo4j-data/neo4j-init.sh copy:/data
docker rm -f copy


#Prepare volume for cbioportal database 
docker volume rm cbioportal_mysql_data_volume
docker volume create cbioportal_mysql_data_volume
docker run -d --name copy --mount source=cbioportal_mysql_data_volume,target=/data nginx
docker cp cbioportal/mysql-snapshots/initialized_2_studies/cbipportal_mysql.tgz copy:/
docker exec copy /bin/bash -c "tar -xf /cbipportal_mysql.tgz -C /data"
docker rm -f copy

# Start the dataportal stack
docker-compose up -d --no-recreate

# Wait untill stack is up

attempt_counter=0
max_attempts=10

until $(curl --insecure --output /dev/null --silent --head --fail http://dataportal:8080/data/home); do
    if [ ${attempt_counter} -eq ${max_attempts} ];then
      echo "Max attempts reached"
      exit 1
    fi

    printf '.'
    attempt_counter=$(($attempt_counter+1))
    sleep 5
done

# DS - init DataHub
sleep 30

docker exec  datahub bash -c "sed -i 's/localhost/db/g' config.py"
#docker exec datahub ./init_datahub_from_container.sh

docker exec datahub /api/init_db.py
docker exec datahub /api/populate_db.py
docker exec datahub /api/expressions_create_cache.py

# Restart proxy container
cd $COMPOSE_DIR
docker-compose restart proxy
