import os
import json

from app import app
from config import Config


def init_folder(folder):
    if not (os.path.isdir(folder)):
        os.mkdir(folder)
    else:
        for filename in os.listdir(folder):
            file_path = os.path.join(folder, filename)
            try:
                if os.path.isfile(file_path):
                    os.unlink(file_path)
                elif os.path.isdir(file_path):
                    shutil.rmtree(file_path)
            except Exception as e:
                print('Failed to delete %s. Reason: %s' % (file_path, e))


def set_status_value(status_value):
    if status_value in app.config['STATUS_VALUES']:
        status_dict = {"status": status_value}
        with open(app.config['STATUS_FILE'], 'w') as sf:
            json.dump(status_dict, sf)
    else:
        raise Exception(status_value + " is not a valid status value.")

def get_status_value():
    with open(app.config['STATUS_FILE'], 'r') as sf:
        status_dict = json.load(sf)
        return status_dict['status']
