import logging
import os
from app import app
from app.tools import set_status_value

if __name__ == '__main__':

    if not os.path.isfile(app.config['STATUS_FILE']):
        set_status_value("running")
    logging.basicConfig(level=logging.INFO, handlers=[logging.FileHandler("log/flask.log"), logging.StreamHandler()])
    app.run(debug=True, host='0.0.0.0', port=5001)


