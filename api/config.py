import os
basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    PREFIX = '/api'
    USAGE_INFO_ADDRESS = 'https://gitlab.ics.muni.cz/europdx/sandbox/sandbox-api/-/blob/master/README.md'
    UPLOAD_FOLDER = '/tmp/upload'
    UPLOAD_FILE = 'europdx.tgz'
    STATUS_FILE = 'status.json'
    STATUS_VALUES = ['running', 'importing', 'resetting', 'down', 'failed', 'failed_import', 'failed_reset']
    IMPORT_FOLDER = '/tmp/import'
    IMPORT_STDOUT = 'log/import_stdout.log'
    IMPORT_STDERR = 'log/import_stderr.log'
    RESET_STDOUT = 'log/reset_stdout.log'
    RESET_STDERR = 'log/reset_stderr.log'
    REDIS_URL = 'redis://redis:6379'
