# Starting sandbox-galaxy-docker

```diff
- ToDo: Upozorneni na naroky na HW - budou se stahovat desitky GB, vypocentni narocnost -> disk, RAM, CPU
```

1. Clone source repository
   ```
   git clone https://gitlab.ics.muni.cz/europdx/sandbox/sandbox-galaxy-docker.git
   cd sandbox-galaxy-docker
   ```
2. Set variable HOST in .env file to FQDN name of your server and ADMIN_PASSWORD to your preferred password.

```diff
- ToDo: Zminit tam taky, ze je dobre si nastavit REFERNECE_DATA na nejaky velky disk
```
3. SSL certificates (optional) - There is self-signed SSL certificate already in the repository to make the the start as easy as possible. You can also use you own SSL cerfificate signed by your favourite certificate authority, if you already have one. Just switch to folder proxy/ssl and replace self-signed key pair in files fullchain.pem and privkey.pem with SSL key pair created for FQDN of your machine.
If you prefere not to use self-signed SSL certificate and do not have SSL certificate for your machine, the Let's encrypt SSL certificate can be created. There is a script prepared in swag folder for this case. Just follow this steps:
    1. Switch to swag directory 
       ```
       cd swag
       ```
    2. Launch ./get_certs.sh script
       ```
       sudo ./get_certs.sh
       ```
    
      If everything goes well, new key pair and certificate is created and installed in /proxy/ssl directory. Otherwise self-signed SSL certificate is used.

4. Prepare Sandbox's internal data files
   ```
   ./prepare_data.sh
   ```

5. Copy NGS data to galaxy/import or change IMPORT_DIR in .env
  If you want to test the import, you can download the samples on the ```http://download.edirex.ics.muni.cz/data/test-data/test/``` into the ./galaxy/import directory and keep the default settings.

6. Start whole environment  
   ```
   docker-compose up -d
   ```

7. Go to ```https://hostname_or_IP_address:8082``` and enter username + password (set in step 2.)

8. Click on "Login or Register" from top panel and log in to Galaxy app with username  *admin* and password *password*

9. Import your data

If you do not wish to use your own data, skipt this step and download the test data according the step 10.

Select Shared Data / Data Libraries from top panel.

<p align="center">
  <img src="images/data_lib_1.png" width="800" title="Select data libraries from top panel.">
</p>


Select ```+ Library``` and give the new library a name and description, and click on the save icon.

<p align="center">
  <img src="images/data_lib_2.png" width="800" title="hover text">
</p>


Click on the name of the newly created library and then select ```+ Datasets``` from import directory.

<p align="center">
  <img src="images/data_lib_3.png" width="800" alt="accessibility text">
</p>


Select your datasets and ```import```.

<p align="center">
  <img src="images/data_lib_4.png" width="800" alt="accessibility text">
</p>


Select your datasets in the library and ```Export to History``` as Datasets or as a collection.
If your data are homogenous, eg. single-end or paired-end Fastq files, it is more useful to use a collection as the workflows will consume the entire collection and process all of the samples simultaneously.

<p align="center">
  <img src="images/data_lib_5.png" width="800" alt="accessibility text">
</p>


If you wish to process Pair-end data, you should select List of Pairs as the type of the collection.
In the next step you will see this window, where you have to specify the regex by which the forward and reverse files are distinguished. The default is _1 and _2.

<p align="center">
  <img src="images/data_lib_6.png" width="800" alt="accessibility text">
</p>

After specifying the name and creating the collection, the datasets will appear in the selected / newly created History.


10. Download the reference data and indexes

Select the QUICK START tool from the Prepare Reference Data section and select which indexes you want to download.

Optional but recommended: select also the Testing data, as it also downloads the auxiliary inputs needed to run the workflow.

<p align="center">
  <img src="images/quick_start_1.png" width="800" alt="accessibility text">
</p>

```diff
- ToDo:
- jak vyplnit metadata, sampleplatformdata, diagnosis_mapping (testovaci data lze vzit tak jak jsou)
```

To run the workflow, select the Workflow option from the top panel, and then select the workflow you wish to run.
<p align="center">
  <img src="images/workflow_1.png" width="800" alt="accessibility text">
</p>
The inputs should look exactly like that on the figure above. If you are running the RNA-seq worflow, select RNA_sampleplatform.xslx in the sampleplatform.xslx field. If you wish to run the Variant calling workflow, select the MUT_sampleplatform.xslx instead. Note that Metadata, Samlpleplatform and Diagnosis mappings files have to be manualy changed to fit your data. 

```diff
- ToDo: 
- pridat zdroj informacii ako vyplnit Metadata.

More info on how to fill the template metadata files ?here?.
```

# Re-starting sandbox-galaxy-docker

```diff
- ToDo: Do budoucna asi promylset, jak to udelat, aby po restartu Galaxy tam zustaly data
```

1. Stop sandbox-galaxy-docker
   ```
   docker-compose down
   docker volume prune
   ```
2. Prepare data files
   ```
   ./delete_previous_data.sh
   ./prepare_data.sh
   ```
2. Start whole environment  
   ```
   docker-compose up -d
   ```



# App endpoints
- Cbioportal: https://*hostname_or_IP_address*:8080
- Galaxy: https://*hostname_or_IP_address*:8082
- Dataportal: https://*hostname_or_IP_address*
- Datahub API: https://*hostname_or_IP_address*:5000
- Sandbox API: http://*hostname_or_IP_address*:5001
- Backend access: https://*hostname_or_IP_address*:8085
  - ?? chce to login / heslo

# SSL certificates (optional)
There are self-signed SSL key and certificate in the repository to make the the start as easy as possible. You can generate SSL key pair for your specific host name a put it in proxy/ssl and replace self-signed pair in files fullchain.pem and privkey.pem.

# Sandbox API usage

## Resources

* /api/status
* /api/log/import
* /api/log/reset
* /api/upload
* /api/import
* /api/reset

## Status values
* running
* importing
* resetting
* down
* failed
* failed_import
* failed_reset

## Sandbox API Usage
### Check status
<code>curl -H "Authorization: Bearer secret-token" -k https://FQDN:5001/api/status</code>


### Import log
<code>curl -H "Authorization: Bearer secret-token" -k https://FQDN:5001/api/log/import</code>

### Reset log
<code>curl -H "Authorization: Bearer secret-token" -k https://FQDN:5001/api/log/reset</code>


### File upload
<code>curl --form file=@europdx.tgz -H "Authorization: Bearer secret-token" -X POST -k https://FQDN:5001/api/upload</code>

### Init data import
<code>curl -X POST -H "Authorization: Bearer secret-token" -k https://FQDN:5001/api/import</code>

### Init sendbox reset to default
<code>curl -X POST -H "Authorization: Bearer secret-token" -k https://FQDN:5001/api/reset</code>

<br>

*Note:* ```-k``` *is there to disable cURL SSL certificate verification*

## Example
<code>curl -H "Authorization: Bearer secret-token" -k http://localhost:5001/api/status</code>
