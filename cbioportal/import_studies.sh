#!/usr/bin/env bash

set -x

COMPOSE_DIR=/sandbox-galaxy-docker

for f in ./study/*/
do
	echo "Processing $f"
	#docker exec -it cbioportal-container metaImport.py -u http://localhost:8080 -s $f -o
        docker exec -i cbioportal metaImport.py -u http://cbioportal:8080 -s $f -o
done

#docker exec -it cbioportal-container metaImport.py -u http://localhost:8080 -s /study/lgg_ucsf_2014/ -o
cd $COMPOSE_DIR
docker-compose restart cbioportal
