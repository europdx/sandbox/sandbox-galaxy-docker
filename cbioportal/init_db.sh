#!/usr/bin/env bash
# Start cBioPortal with init DB
for d in data; do
    cd $d; ./init.sh
    cd ..
done
